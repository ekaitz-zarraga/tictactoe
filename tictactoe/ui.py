from .gamestate import GameState
from textwrap import dedent

def display_board(gamestate: GameState) -> str:
    contents = [i if i is not None else " " for i in gamestate.state]
    print(dedent(f"""
     {contents[0]} | {contents[1]} | {contents[2]}
    ---+---+---
     {contents[3]} | {contents[4]} | {contents[5]}
    ---+---+---
     {contents[6]} | {contents[7]} | {contents[8]}
    """))

def input_coord(c):
    while True:
        coord = input(f"Posición en {c}>")
        try:
            coord = int(coord)
            return coord
        except:
            print("Introduce un número!")
            pass

def warn_out_of_bounds():
    print("Estás escribiendo fuera del tablero!!")

def warn_position_taken():
    print("La posición ya está en uso")

def greet_winner(winner):
    print("Ganador " + winner)

def display_draw():
    print("La partida ha terminado en empate")
