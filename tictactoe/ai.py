"""
From: https://www.neverstopbuilding.com/blog/minimax
A description for the algorithm, assuming X is the "turn taking player," would look something like:

If the game is over, return the score from X's perspective.
Otherwise get a list of new game states for every possible move
Create a scores list
For each of these states add the minimax result of that state to the scores list
If it's X's turn, return the maximum score from the scores list
If it's O's turn, return the minimum score from the scores list
"""
from .gamestate import GameState

class Node:
    def __init__(self, value, x=None, y=None):
        self.children = []
        self.value = value
        self.score = None
        self.x = x
        self.y = y
    def __repr__(self):
        return f"<Node: score={self.score} value={self.value} children={self.children}>"
    def append_child(self, child):
        self.children.append(child)

class AI:
    def __init__(self, me : str, game : GameState):
        self.me = me
        self.tree = Node( game.copy() )
        AI.expand_node(self.tree)
        self.calc_scores()

    def __repr__(self):
        return f"<AI me={self.me} tree={self.tree}>"

    def expand_node(node):
        for x,y,gs in node.value.branches():
            node.append_child(AI.expand_node(Node(gs,x,y)))
        return node

    def move(self):
        ## Maxes in the possible options
        decision = max(self.tree.children, key=lambda x: x.score)
        self.tree = decision
        return decision.x, decision.y

    def oponent_moved_to(self, gs):
        ## Find which of the children is the one that the oponent chose
        for i in self.tree.children:
            if i.value.state == gs.state:
                self.tree = i

    def calc_scores(self):
        # No hacemos recursivo (por probar)
        node0 = self.tree
        i = 0
        pending_nodes = { i: [node0] }
        while True:
            i += 1
            pending_nodes[i] = []
            for node in pending_nodes[i-1]:
                pending_nodes[i] += node.children
            if pending_nodes[i] == []:
                break

        levels = sorted(pending_nodes.keys(), reverse=True)
        for level in levels:
            for node in pending_nodes[level]:
                self.write_node_score(node)

    def write_node_score(self, node):
        if len(node.children) == 0:
            winner = node.value.won()
            if winner is None:
                node.score = 0
            else:
                node.score = 1 if winner == self.me else -1
        elif len(node.children) == 1:
            node.score = node.children[0].score
        else:
            # Esto es el minimax:
            if self.me == node.value.current:
                node.score = max(node.children, key=lambda x: x.score).score
            else:
                node.score = min(node.children, key=lambda x: x.score).score
