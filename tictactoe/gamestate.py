from __future__ import annotations
from typing import List, Tuple, Iterator

class OutOfBounds(Exception):
    pass
class PositionTaken(Exception):
    pass

class GameState:
    def __init__(self) -> None:
        self.current : str = "O"
        self.state : List = [None] * 9

    def copy(self) -> GameState:
        new_gs = GameState()
        new_gs.current = self.current
        new_gs.state = self.state.copy()
        return new_gs

    def branches(self) -> Iterator[ Tuple[int, int, GameState] ]:
        if self.finished():
            return
        for i, pos in enumerate(self.state):
            if pos is None:
                new_gs = self.copy()
                new_gs.state[i] = self.current
                new_gs.next()
                yield (i%3, int((i-i%3)/3), new_gs)

    def set(self, x: int, y: int) -> None:
        if x >= 3 or x < 0 or y < 0 or y >= 3:
            raise OutOfBounds()
        if self.state[y*3+x] is not None:
            raise PositionTaken()
        self.state[y*3+x] = self.current
        self.next()

    def next(self)->None:
        self.current = "O" if self.current == "X" else "X"

    def finished(self) -> bool:
        return bool(self.won()) or all(self.state)

    def won(self) -> str | None:
        c = self.state
        if c[0] == c[4] == c[8]:
            return c[4]
        if c[2] == c[4] == c[6]:
            return c[4]
        if c[0] == c[3] == c[6]:
            return c[0]
        if c[1] == c[4] == c[7]:
            return c[1]
        if c[2] == c[5] == c[8]:
            return c[2]
        if c[0] == c[1] == c[2]:
            return c[0]
        if c[3] == c[4] == c[5]:
            return c[3]
        if c[6] == c[7] == c[8]:
            return c[6]
        return None

    def __repr__(self) -> str:
        return f"<GameState current={self.current} state={self.state}>"
