from tictactoe import ui
from tictactoe.gamestate import GameState, OutOfBounds, PositionTaken
from tictactoe.ai import AI

if __name__ == "__main__":
    gs = GameState()
    ai = AI("X", gs)
    while True:
        ui.display_board(gs)
        while True:
            x = ui.input_coord("X")
            y = ui.input_coord("Y")
            try:
                gs.set(x, y)
                break
            except OutOfBounds:
                ui.warn_out_of_bounds()
            except PositionTaken:
                ui.warn_position_taken()
        if gs.finished():
            break
        ai.oponent_moved_to(gs)
        x, y = ai.move()
        gs.set(x,y)
        if gs.finished():
            break

    ui.display_board(gs)
    winner = gs.won()
    if winner:
        ui.greet_winner(gs.won())
    else:
        ui.display_draw()
